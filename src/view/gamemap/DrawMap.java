package view.gamemap;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.LinkedList;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

import data.Colors;
import data.PositionCoordinates;
import view.buttons.ButtonImpl;
/**
 * This class draws the world map with the colored buttons related to players
 *
 */
public class DrawMap {
	private final static int ONE=1;
	
	public static void setMap(JPanel w,LinkedList<ButtonImpl> listabottoni) {

		int index=0;
		GridBagLayout layout=new GridBagLayout();
		GridBagConstraints c=new GridBagConstraints();
		w.setLayout(layout);
		
	    //alaska
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.ALASKA.getGridx();
		c.gridy=PositionCoordinates.ALASKA.getGridy();
		c.anchor=GridBagConstraints.SOUTH;
		c.weightx=ONE;
		c.weighty=ONE;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//nordwestterritories 
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.NORTHWESTTERRITORIES.getGridx();
		c.gridy=PositionCoordinates.NORTHWESTTERRITORIES.getGridy();
		c.anchor=GridBagConstraints.SOUTH;
		layout.setConstraints(listabottoni.get(index) , c);
		w.add(listabottoni.get(index));
		index++;
		
		//greenland
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.GREENLAND.getGridx();
		c.gridy=PositionCoordinates.GREENLAND.getGridy();
		c.anchor=GridBagConstraints.SOUTH;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//alberta
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.ALBERTA.getGridx();
		c.gridy=PositionCoordinates.ALBERTA.getGridy();
		c.anchor=GridBagConstraints.EAST;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//ontario 
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.ONTARIO.getGridx();
		c.gridy=PositionCoordinates.ONTARIO.getGridy();
		c.anchor=GridBagConstraints.CENTER;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//quebec 
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.QUEBEC.getGridx();
		c.gridy=PositionCoordinates.QUEBEC.getGridy();
		c.anchor=GridBagConstraints.WEST;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//eastern united states 
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.EASTERNUNITEDSTATES.getGridx();
		c.gridy=PositionCoordinates.EASTERNUNITEDSTATES.getGridy();
		c.anchor=GridBagConstraints.SOUTHEAST;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//western united states 
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.WESTERNUNITEDSTATES.getGridx();
		c.gridy=PositionCoordinates.WESTERNUNITEDSTATES.getGridy();
		c.anchor=GridBagConstraints.NORTH;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//central america 
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.CENTRALAMERICA.getGridx();
		c.gridy=PositionCoordinates.CENTRALAMERICA.getGridy();
		c.anchor=GridBagConstraints.CENTER;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;

		//venezuela 
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.VENEZUELA.getGridx();
		c.gridy=PositionCoordinates.VENEZUELA.getGridy();
		c.anchor=GridBagConstraints.SOUTHEAST;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//peru 
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.PERU.getGridx();
		c.gridy=PositionCoordinates.PERU.getGridy();
		c.anchor=GridBagConstraints.CENTER;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//brazil
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.BRAZIL.getGridx();
		c.gridy=PositionCoordinates.BRAZIL.getGridy();
		c.anchor=GridBagConstraints.CENTER;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//argentina
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.ARGENTINA.getGridx();
		c.gridy=PositionCoordinates.ARGENTINA.getGridy();
		c.anchor=GridBagConstraints.EAST;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//iceland
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.ICELAND.getGridx();
		c.gridy=PositionCoordinates.ICELAND.getGridy();
		c.anchor=GridBagConstraints.SOUTH;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//great britain
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.GREITBRITAIN.getGridx();
		c.gridy=PositionCoordinates.GREITBRITAIN.getGridy();
		c.anchor=GridBagConstraints.WEST;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//scandinavia
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.SCANDINAVIA.getGridx();
		c.gridy=PositionCoordinates.SCANDINAVIA.getGridy();
		c.anchor=GridBagConstraints.NORTHEAST;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//ukraine
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.UKRAINE.getGridx();
		c.gridy=PositionCoordinates.UKRAINE.getGridy();
		c.anchor=GridBagConstraints.CENTER;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//western europe
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.WESTERNEUROPE.getGridx();
		c.gridy=PositionCoordinates.WESTERNEUROPE.getGridy();
		c.anchor=GridBagConstraints.NORTHWEST;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//northern europe
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.NORTHERNEUROPE.getGridx();
		c.gridy=PositionCoordinates.NORTHERNEUROPE.getGridy();
		c.anchor=GridBagConstraints.SOUTHWEST;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//southern europe
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.SOUTHERNEUROPE.getGridx();
		c.gridy=PositionCoordinates.SOUTHERNEUROPE.getGridy();
		c.anchor=GridBagConstraints.EAST;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//northern africa
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.NORTHERNAFRICA.getGridx();
		c.gridy=PositionCoordinates.NORTHERNAFRICA.getGridy();
		c.anchor=GridBagConstraints.NORTHWEST;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//egypt
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.EGYPT.getGridx();
		c.gridy=PositionCoordinates.EGYPT.getGridy();
		c.anchor=GridBagConstraints.NORTHWEST;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//congo
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.CONGO.getGridx();
		c.gridy=PositionCoordinates.CONGO.getGridy();
		c.anchor=GridBagConstraints.SOUTHEAST;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//eastern africa
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.EASTERNAFRICA.getGridx();
		c.gridy=PositionCoordinates.EASTERNAFRICA.getGridy();
		c.anchor=GridBagConstraints.CENTER;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//southern africa
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.SOUTHERNAFRICA.getGridx();
		c.gridy=PositionCoordinates.SOUTHERNAFRICA.getGridy();
		c.anchor=GridBagConstraints.EAST;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//madagascar
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.MADAGASCAR.getGridx();
		c.gridy=PositionCoordinates.MADAGASCAR.getGridy();
		c.anchor=GridBagConstraints.NORTH;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//siberia
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.SIBERIA.getGridx();
		c.gridy=PositionCoordinates.SIBERIA.getGridy();
		c.anchor=GridBagConstraints.NORTH;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//jacuzia
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.JACUZIA.getGridx();
		c.gridy=PositionCoordinates.JACUZIA.getGridy();
		c.anchor=GridBagConstraints.SOUTHEAST;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//kamchatka
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.KAMCHATKA.getGridx();
		c.gridy=PositionCoordinates.KAMCHATKA.getGridy();
		c.anchor=GridBagConstraints.SOUTH;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//urals
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.URALS.getGridx();
		c.gridy=PositionCoordinates.URALS.getGridy();
		c.anchor=GridBagConstraints.WEST;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//cita
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.CITA.getGridx();
		c.gridy=PositionCoordinates.CITA.getGridy();
		c.anchor=GridBagConstraints.EAST;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//japan
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.JAPAN.getGridx();
		c.gridy=PositionCoordinates.JAPAN.getGridy();
		c.anchor=GridBagConstraints.WEST;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//afghanistan
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.AFGHANISTAN.getGridx();
		c.gridy=PositionCoordinates.AFGHANISTAN.getGridy();
		c.anchor=GridBagConstraints.NORTHWEST;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//mongolia
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.MONGOLIA.getGridx();
		c.gridy=PositionCoordinates.MONGOLIA.getGridy();
		c.anchor=GridBagConstraints.CENTER;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//china
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.CHINA.getGridx();
		c.gridy=PositionCoordinates.CHINA.getGridy();
		c.anchor=GridBagConstraints.NORTH;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//middle east
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.MIDDLEEAST.getGridx();
		c.gridy=PositionCoordinates.MIDDLEEAST.getGridy();
		c.anchor=GridBagConstraints.SOUTHEAST;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//india
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.INDIA.getGridx();
		c.gridy=PositionCoordinates.INDIA.getGridy();
		c.anchor=GridBagConstraints.SOUTHEAST;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//siam
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.SIAM.getGridx();
		c.gridy=PositionCoordinates.SIAM.getGridy();
		c.anchor=GridBagConstraints.SOUTH;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//indonesia
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.INDONESIA.getGridx();
		c.gridy=PositionCoordinates.INDONESIA.getGridy();
		c.anchor=GridBagConstraints.EAST;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//new guinea
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.NEWGUINEA.getGridx();
		c.gridy=PositionCoordinates.NEWGUINEA.getGridy();
		c.anchor=GridBagConstraints.CENTER;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//western australia
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.WESTERNAUSTRALIA.getGridx();
		c.gridy=PositionCoordinates.WESTERNAUSTRALIA.getGridy();
		c.anchor=GridBagConstraints.CENTER;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));
		index++;
		
		//eastern australia
		listabottoni.get(index).setBorderPainted(false);
		listabottoni.get(index).setBackground(Colors.values()[listabottoni.get(index).getOwner()].getColor());
		c.gridx=PositionCoordinates.EASTERNAUSTRALIA.getGridx();
		c.gridy=PositionCoordinates.EASTERNAUSTRALIA.getGridy();
		c.anchor=GridBagConstraints.CENTER;
		layout.setConstraints(listabottoni.get(index), c);
		w.add(listabottoni.get(index));	
	}

}
