package view.buttons;

import java.awt.Color;

import javax.swing.JButton;

import controller.Controller;
import data.Colors;
import view.gamemap.DrawMap;

public class ButtonImpl extends JButton {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2265036808756473910L;
	private final int code;
	private int numArmies = 1;
	private int owner;

    public ButtonImpl(final int code) {
		this.code = code;
		this.setText("1");
		this.setOwner(0);
		this.setEnabled(true);
	}
    
    public final int getCode() {
    	return this.code;
    }

	public final int getOwner() {
		return this.owner;
	}

	public final void setOwner(final int n) {
		this.owner = n;
		this.setBackground(Colors.values()[n].getColor());
	}

	public final int getNumArmiesOn() {
		return this.numArmies;
	}

	public final void setNumArmiesOn(final int n) {
		this.setText(String.valueOf(n));
	}

	public final void addNumArmiesOn(final int n) {
		this.setText(String.valueOf(getNumArmiesOn() + n));
	}

	public final void subNumArmiesOn(final int n) {
		this.setText(String.valueOf(getNumArmiesOn() - n));
	}
	

}
