package view.buttons;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import controller.Controller;

public class ButtonListCreator {

	private LinkedList<ButtonImpl> listabottoni = new LinkedList<ButtonImpl>();

	public ButtonListCreator() {
		
		for (int i = 0; i < 42; i++) {
            listabottoni.add(new ButtonImpl(i + 1));
    	}

		listabottoni.forEach(a -> {
            a.addActionListener(new ActionListener() {
            	public void actionPerformed(ActionEvent e) {
            		Controller.passButton(a.getCode());
            	}
            });
        });
	}
	
	public LinkedList<ButtonImpl> getListaBottoni() {
		return listabottoni;
	}
	
}
