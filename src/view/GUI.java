package view;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

import controller.Controller;
import hud.SetUpHud;
import model.Land;
import players.Player;
import view.buttons.ButtonImpl;
import view.buttons.ButtonListCreator;
import view.gamemap.DrawMap;
import view.gamemap.WorldGUI;
/**
 * 
 * Creation of the graphic interface with the various components
 *
 */
public class GUI extends JFrame implements GUIFactory{
	private List<String> carte=Arrays.asList();
	private JPanel w=new WorldGUI("risiko.png");
	private static ButtonListCreator creatorelistabottoni;
	private static LinkedList<ButtonImpl> listabottoni;
	
	public GUI() {
		creatorelistabottoni = new ButtonListCreator();
		listabottoni = creatorelistabottoni.getListaBottoni();
	    this.setDefaultCloseOperation(this.EXIT_ON_CLOSE);
	    this.setTitle("Risiko");
	    int width = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth();
	    int height = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();
	    this.addMap(w);
	    this.addCardsGUI(ViewCards.getViewCards());
	    ViewCards.updatePanel(carte);
	    SetUpHud hudGioc = new SetUpHud();
	    this.addPlayerGUI(hudGioc.getHUD());
	    SetUpHud.updateHUD(controller.Controller.getActualPlayer());
	    SetUpHud.updateDice(new int[] {0, 0, 0}, new int[] {0, 0, 0});
	    this.setSize(width, height);
	    DrawMap.setMap(w, listabottoni); 
	    //this.pack();
	    this.setVisible(true);
	}

	@Override
	public JFrame getFrame() {
		return this;
	}

	@Override
	public void addPlayerGUI(JPanel panel) {
		this.getContentPane().add(panel,BorderLayout.SOUTH);
		
	}

	@Override
	public void addCardsGUI(JPanel panel) {
		this.getContentPane().add(panel, BorderLayout.EAST);
		
	}

	@Override
	public void addMap(JPanel panel) {
		this.getContentPane().add(panel, BorderLayout.CENTER);
		
	}
	
	public void activateButtonsPlayerInTurn(final Player p) {
		
		for (ButtonImpl a : listabottoni) {
			if (a.getOwner() == p.getColore()) {
				a.setEnabled(true);
			} else {
				a.setEnabled(false);
			}
		}

	}
	
	public void deactivateAllButtons() {
		for (ButtonImpl a : listabottoni) {
			a.setEnabled(false);
		}
	}
	
	public void activateButton(final int code) {
		listabottoni.get(code - 1).setEnabled(true);
	}
	
	public void deactivateButton(final int code) {
		listabottoni.get(code - 1).setEnabled(false);
	}
	
	public void updateArmiesButton(final int i, final int a) {
		listabottoni.get(i).setNumArmiesOn(a);
	}
	
	public void updateColorButton(final int i, final int a) {
		listabottoni.get(i).setOwner(a);
	}
	
}
