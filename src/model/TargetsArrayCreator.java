package model;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Random;

/**
 * this class creates the targets array and associate one of it with each player .
 *
 */
public final class TargetsArrayCreator {

	private final int MAXDICE = 6;
	private final int MINDICE = 1;
	private int[] targetsArray;
	private static TargetsArrayCreator singleton;
	private TargetsDefine tar;

	/**
	 * 
	 * @param numPlayers is the size of the list players .
	 */
	public TargetsArrayCreator(final int numPlayers, final Continent continenti) {
		targetsArray = new int [numPlayers];
		Random rand = new Random();
		for (int i = 0; i < numPlayers; i++) {
			targetsArray[i] = rand.nextInt(MAXDICE) + MINDICE;
		}
		tar = TargetsDefine.getTar(continenti);
	}
	
	public static TargetsArrayCreator getTargetsImpl(final int numPlayers, final Continent continenti) {
		if (singleton == null) {
			singleton = new TargetsArrayCreator(numPlayers, continenti);
		}
		return singleton;
	}
	
	
	/**
	 * 
	 * @return targetsArray is the array with inside the targets associated to each player.
	 */
	public int [] getTargetsArray() {
		return targetsArray;
	}
	
	public TargetsDefine getTargets() {
		return tar;
	}

}
