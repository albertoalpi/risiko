package model;

public class LandImpl implements Land {

	private final String nome;
	private final int code;
	private int numArmies = 1;
	private int owner = 0;
    private final int[] confini;

    LandImpl(final String nome, final int code, final int[] confini) {
        this.nome = nome;
        this.code = code;
        this.confini = confini;
    }

    @Override
	public String getName() {
		return this.nome;
	}

	@Override
	public int getCode() {
		return this.code;
	}

	@Override
	public int[] getConfini() {
		return this.confini;
	}

	@Override
	public int getNumArmies() {
		return numArmies;
	}

	@Override
	public void setNumArmies(final int numArmies) {
		this.numArmies = numArmies;
	}

	@Override
	public int getOwner() {
		return owner;
	}

	@Override
	public void setOwner(final int owner) {
		this.owner = owner;
	}
	
	

}
