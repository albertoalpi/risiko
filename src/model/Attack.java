package model;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import controller.Controller;
import players.CircList;
import players.Player;
import players.PlayerImpl;

/**
 * 
 * this method is used to control the attack between two country.
 *
 */
public class Attack {

	private int numArmiesAtk = 0;
	private int numArmiesDef = 0;
	private static Attack singleton;
	private int[] NumsAtk = new int[3];
	private int[] NumsDef = new int[3];
	private final int MAXDICE = 6;
	private final int MINDICE = 1;
	private static int NumAtkWin = 0;
	private static int NumDefWin = 0;

	public Attack () {
		
	}
	
	public static Attack getAttack () {
		if (singleton == null) {
			singleton = new Attack();
		}
		return singleton;
	}
	
	/**
	 * @param a is the attacker country.
	 * @param b is the attacked country.
	 * @param pp is the list of the players.
	 */
	public void atk (final List<Land> terr, final int a, final int b, final CircList<PlayerImpl> pp, final Check controlli) {
		numArmiesAtk = 0;
		numArmiesDef = 0;

		numArmiesAtk = terr.get(a - 1).getNumArmies() - 1; // numero possibili armate che possono attaccare

		numArmiesAtk = normalize(numArmiesAtk);
		numArmiesDef = normalize(terr.get(b - 1).getNumArmies());

		RNG(a, b, numArmiesAtk, numArmiesDef, terr);

		if (controlli.isDead(b)) {

			if (terr.stream().filter(x -> x.getOwner() == terr.get(b - 1).getOwner()).count() == 1) {
				for (Player p: pp) {
					if (p.getColore() == terr.get(b - 1).getOwner()) {
						pp.remove(p);
						break;
					}
				}
			}

			terr.get(b - 1).setOwner(terr.get(a - 1).getOwner());
			controlli.move(a, b, numArmiesAtk);

			//controllo se l'obbiettivo di A era uccidere B

//			for(Player p: P) {
//				if(p.getColore()==A.getOwner()) {
//					if(0==0/*obbiettivo A == B died*/) {
//						//caso true fine gioco
//						//A hai vinto!
//						break;
//					}
//					//Caso false avanti
//				}
//			}
		}
	}
	
	public void RNG(final int a, final int b, final int att, final int def, final List<Land> terr) {
		NumAtkWin= 0;
		NumDefWin = 0;
		
		for (int i = 0; i < 3; i++) {
			NumsAtk[i] = 0;
			NumsDef[i] = 0;
		}
		Random rand = new Random();
		for (int i = 0; i < att; i++) {
			NumsAtk[i] = rand.nextInt(MAXDICE) + MINDICE;
		}
		for (int i = 0; i < def; i++) {
			NumsDef[i] = rand.nextInt(MAXDICE) + MINDICE;
		}
		Arrays.sort(NumsAtk);
		Arrays.sort(NumsDef);
		Controller.updateDices(NumsAtk, NumsDef);
		if (att > def) {
			// caso in cui att sia maggiore di def
			for (int i = 2; i >= 3 - def; i--) {
				if (NumsAtk[i] > NumsDef[i]) {
					NumAtkWin++;
				} else {
					NumDefWin++;
				}
			}
		} else {
			// caso in cui def sia maggiore di att
			for (int i = 2; i >= 3 - att; i--) {
				if (NumsAtk[i] > NumsDef[i]) {
					NumAtkWin++;
				} else {
					NumDefWin++;
				}
			}
		} 
		terr.get(a - 1).setNumArmies(terr.get(a - 1).getNumArmies() - NumDefWin);
		terr.get(b - 1).setNumArmies(terr.get(b - 1).getNumArmies() - NumAtkWin);
		
		Controller.updateArmiesButton(a - 1, terr.get(a - 1).getNumArmies());
		Controller.updateArmiesButton(b - 1, terr.get(b - 1).getNumArmies());
	}

	
	/**
	 * 
	 * @param numarmies is the number of armies can attack.
	 * @return max 3 armies in case numarmies is 3 or more.
	 */
	private int normalize(int numarmies) {

		if (numarmies > 3) {
			numarmies = 3; // normalizzo a 3 nel caso ce ne siano di piu
		}

		return numarmies;
	}

}
