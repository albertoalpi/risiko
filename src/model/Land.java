package model;

public interface Land {
	
	public String getName();
	public int getCode();
	public int[] getConfini();
	public int getNumArmies();
	public void setNumArmies(int numArmies);
	public int getOwner();
	public void setOwner(int owner);

}
