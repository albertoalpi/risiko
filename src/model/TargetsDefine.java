package model;

import java.util.LinkedList;
import java.util.List;

import controller.Controller;
import players.Player;

/**
 * 
 *  this class describe the six method with the specific checks to define if a target has been reached .
 */
public final class TargetsDefine {
	
	private Continent cont;
	private static TargetsDefine singleton;

	public TargetsDefine(final Continent continenti) {
		cont = continenti;
	}
	
	/**
	 * 
	 * @return if target has already been created.
	 */
    public static TargetsDefine getTar(final Continent continenti) {
		if (singleton == null) {
			singleton = new TargetsDefine(continenti);
		}
		return singleton;
	}
	
	/**
	 * @param ter is the list of country
	 * @param p is the player in turn
	 * @return true if target reached else false.
	 */
	public boolean AmN_Af(final List<Land> ter, final Player p) {
		return cont.totalAmericaN(ter, p) && cont.totalAfrica(ter, p);
	}
	
	/**
	 * @param ter is the list of country
	 * @param p is the player in turn
	 * @return true if target reached else false.
	 */
	public boolean AmN_Oc(final List<Land> ter, final Player p) {
		return cont.totalAmericaN(ter, p) && cont.totalOceania(ter, p);
	}
	
	/**
	 * @param ter is the list of country
	 * @param p is the player in turn
	 * @return true if target reached else false.
	 */
	public boolean As_Oc(final List<Land> ter, final Player p) {
		return cont.totalAsia(ter, p) && cont.totalOceania(ter, p);
	}
	
	/**
	 * @param p is the player in turn
	 * @return true if target reached else false.
	 */
	public boolean As_Af(final List<Land> ter, final Player p) {
		return cont.totalAsia(ter, p) && cont.totalAfrica(ter, p);
	}
	
	/**
	 * @param ter is the list of country
	 * @param p is the player in turn
	 * @return true if target reached else false.
	 */
	public boolean Eu_AmS_smt(final List<Land> ter, final Player p) {
		return cont.totalEuropa(ter, p) && cont.totalAmericaS(ter, p) && (cont.totalAfrica(ter, p) || cont.totalAmericaN(ter, p) || cont.totalAsia(ter, p) || cont.totalOceania(ter, p));
	}
	
	/**
	 * @param ter is the list of country
	 * @param p is the player in turn
	 * @return true if target reached else false.
	 */
	public boolean Eu_Oc_smt(final List<Land> ter, final Player p) {
		return cont.totalEuropa(ter, p) && cont.totalOceania(ter, p) && (cont.totalAfrica(ter, p) || cont.totalAmericaN(ter, p) || cont.totalAsia(ter, p) || cont.totalAmericaS(ter, p));
	}
}
