package model;

public enum LandsData{

	ALASKA("Alaska", 1, new int[] {2, 4, 29}),
    TERRITORI_DELNORD_OVEST("Territori del Nord-Ovest", 2, new int[] {1, 3, 4, 5}),
    GROENLANDIA("Groenlandia", 3, new int[] {2, 5, 6, 14}),
    ALBERTA("Alberta", 4, new int[] {1, 2, 5, 7}),
    ONTARIO("Ontario", 5, new int [] {2, 3, 4, 6, 7, 8}),
    QUEBEC("Quebec", 6, new int [] {3, 5, 8}),
    STATI_UNITI_ORIENTALI("Stati Uniti Orientali", 7, new int [] {4, 5, 8, 9}),
    STATI_UNITI_OCCIDENTALI("Stati Uniti Occidentali", 8, new int [] {5, 6, 7, 9}),
    AMERICA_CENTRALE("America Centrale", 9, new int [] {7, 8, 10}),
    VENEZUELA("Venezuela", 10, new int [] {9, 11, 12}),
    PERU("Perù", 11, new int [] {10, 12, 13}),
    BRASILE("Brasile", 12, new int [] {10, 11, 13, 21}),
    ARGENTINA("Argentina", 13, new int [] {11, 12}),
    ISLANDA("Islanda", 14, new int [] {3, 15, 16}),
    GRAN_BRETAGNA("Gran Bretagna", 15, new int [] {14, 16, 18, 19}),
    SCANDINAVIA("Scandinavia", 16, new int [] {14, 15, 17, 19}),
    UCRAINA("Ucraina", 17, new int [] {16, 19, 20, 30, 33, 36}),
    EUROPA_OCCIDENTALE("Europa Occidentale", 18, new int [] {15, 19, 20, 21}),
    EUROPA_SETTENTRIONALE("Europa Settentrionale", 19, new int [] {15, 16, 17, 18, 20}),
    EUROPA_MERIDIONALE("Europa Meridionale", 20, new int [] {17, 18, 19, 21, 22, 36}),
    AFRICA_DEL_NORD("Africa del Nord", 21, new int [] {12, 18, 20, 22, 23, 24}),
    EGITTO("Egitto", 22, new int [] {20, 21, 24, 36}),
    CONGO("Congo", 23, new int [] {21, 24, 25}),
    AFRICA_ORIENTALE("Africa Orientale", 24, new int [] {21, 22, 23, 25, 26}),
    AFRICA_DEL_SUD("Afica del Sud", 25, new int [] {23, 24, 26}),
    MADAGASCAR("Madagascar", 26, new int [] {24, 25}),
    SIBERIA("Siberia", 27, new int [] {28, 30, 31, 34, 35}),
    JACUZIA("Jacuzia", 28, new int [] {27, 29, 31}),
    KAMCHATKA("Kamchatka", 29, new int [] {1, 28, 31, 32, 34}),
    URALI("Urali", 30, new int [] {17, 27, 33, 35}),
    CITA("Cita", 31, new int [] {27, 28, 29, 34}),
    GIAPPONE("Giappone", 32, new int [] {29, 34}),
    AFGANISTAN("Afganistan", 33, new int [] {17, 30, 35, 36}),
    MONGOLIA("Mongolia", 34, new int [] {27, 29, 31, 32, 35,}),
    CINA("Cina", 35, new int [] {27, 30, 33, 34, 36, 37, 38}),
    MEDIO_ORIENTE("Medio Oriente", 36, new int [] {17, 20, 22, 33, 35, 37}),
    INDIA("India", 37, new int [] {35, 36, 38}),
    SIAM("Siam", 38, new int [] {35, 37, 39}),
    INDONESIA("Indonesia", 39, new int [] {38, 40, 41}),
    NUOVA_GUINEA("Nuova Guinea", 40, new int [] {39, 41, 42}),
    AUSTRALIA_OCCIDENTALE("Australia Occidentale", 41, new int [] {39, 40, 42}),
    AUSTRALIA_ORIENTALE("Australia Orientale", 42, new int [] {40, 41});
	
	private final String nome;
	private final int code;
    private final int[] confini;
    
    private LandsData(final String nome, final int code, final int[] confini) {
        this.nome = nome;
        this.code = code;
        this.confini = confini;
    }

	public String getNome() {
		return nome;
	}

	public int getCode() {
		return code;
	}

	public int[] getConfini() {
		return confini;
	}
}
