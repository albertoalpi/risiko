package model;

import java.util.LinkedList;
import java.util.List;

import controller.Controller;
import controller.Restart;
import players.Player;

/**
 * 
 * this class checks if a target has been reached and print the right message .
 *
 */
public class CheckTargets {

	private final int AmN_Af = 1;
	private final int AmN_Oc = 2;
	private final int As_Oc = 3;
	private final int As_Af = 4;
	private final int Eu_AmS_smt = 5;
	private final int Eu_Oc_smt = 6;
	private final String ANA = "Conquista la totalità del Nord America e dell'Africa";
	private final String ANO = "Conquista la totalità del Nord America e dell'Oceania";
	private final String AA = "Conquista la totalità dell'Asia e dell'Africa";
	private final String AO = "Conquista la totalità dell'Asia e dell'Oceania";
	private final String EASMT = "Conquista l'Europa, il Sud America ed un terzo continente a tua scelta";
	private final String EOSMT = "Conquista l'Europa, l'Oceania ed un terzo continente a tua scelta";
	private int check = 0;
	private static CheckTargets singleton;
	private TargetsArrayCreator obj;
	
	public CheckTargets (final TargetsArrayCreator obbiettivi) {
		obj = obbiettivi;
	}

	
	public static CheckTargets getCheckTargets (final TargetsArrayCreator obbiettivi) {
		if (singleton == null) {
			singleton = new CheckTargets(obbiettivi);
		}
		return singleton;
	}
	/**
	 * @param t is the list of country
	 * @param p is the player in turn
	 *  this method check if a target has been reached and in this case stop the game .
	 */
	public void win(final List<Land> t, final Player p, final Check controlli) {

		check = obj.getTargetsArray() [p.getColore() - 1];

		switch (check) {

		case AmN_Af:
			if (obj.getTargets().AmN_Af(t, p)) {
				Controller.deactivateAllButtons();
				Controller.deactivateFineTurno();
				Controller.deactivateSpostamento();
				Restart.restart();
			}
			break;

		case AmN_Oc:
			if (obj.getTargets().AmN_Oc(t, p)) {
				Controller.deactivateAllButtons();
				Controller.deactivateFineTurno();
				Controller.deactivateSpostamento();
				Restart.restart();
			}
			break;

		case As_Oc:
			if (obj.getTargets().As_Oc(t, p)) {
				Controller.deactivateAllButtons();
				Controller.deactivateFineTurno();
				Controller.deactivateSpostamento();
				Restart.restart();
			}
			break;

		case As_Af:
			if (obj.getTargets().As_Af(t, p)) {
				Controller.deactivateAllButtons();
				Controller.deactivateFineTurno();
				Controller.deactivateSpostamento();
				Restart.restart();
			}
			break;

		case Eu_AmS_smt:
			if (obj.getTargets().Eu_AmS_smt(t, p)) {
				Controller.deactivateAllButtons();
				Controller.deactivateFineTurno();
				Controller.deactivateSpostamento();
				Restart.restart();
			}
			break;

		case Eu_Oc_smt:
			if (obj.getTargets().Eu_Oc_smt(t, p)) {
				Controller.deactivateAllButtons();
				Controller.deactivateFineTurno();
				Controller.deactivateSpostamento();
				Restart.restart();
			}
			break;

		default: //c'è stato qualche errore
			break;

		}
	
	}
	
	/**
	 * @param in is the color of the player (code of the player) .
	 * @return the message to print in HUD .
	 */
	public String getString(final int in) {

		check = obj.getTargetsArray() [in - 1];

		switch (check) {

		case AmN_Af:
			return ANA;
		case AmN_Oc:
			return ANO;
		case As_Oc:
			return AO;
		case As_Af:
			return AA;
		case Eu_AmS_smt:
			return EASMT;
		case Eu_Oc_smt:
			return EOSMT;
		default: //c'è stato qualche errore
			return "ERRORE";
		}
	}

}
