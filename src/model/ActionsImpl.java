package model;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import controller.Controller;
import players.Player;

/**
 * 
 * this class implements actions and is used to understand what a pressed botton
 * does it means and what the game must to do .
 *
 */
public final class ActionsImpl implements Actions {

	private static final int PRIMOTURNO = 1;
	private static final int POSIZIONAMENTOINIZIOTURNO = 2;
	private static final int primasceltaattacco = 3;
	private static final int secondasceltaattacco = 4;
	private static final int primasceltaspostamento = 5;
	private static final int secondasceltaspostamento = 6;
	private static final int posizionamentodopoattacco = 7;
	private static final int totaleterritori = 42;
	private static final int armatecadaunotregiocatori = 35;
	private static final int armatecadaunoquattrogiocatori = 30;
	private static final int armatecadaunocinquegiocatori = 25;
	private static final int armatecadaunoseigiocatori = 20;
	private static final int treplayers = 3;
	private static final int quattroplayers = 4;
	private static final int cinqueplayers = 5;
	private static final int seiplayers = 6;
	private static final String MESSAGGIODIPOSIZIONAMENTO = "Premi lo stato dove vuoi aggiungere una armata";
	private int codicestatoattaccante, codicestatospostante;
	private int numerostatiprimaattacco;
	private int numerostatidopoattacco;
	private int armatedaaggiungere;
	private int armatemancanti;
	private int intentodelplayer;
	private static Actions singleton;
	private TargetsArrayCreator obbiettivi;
	private int fasiprimadigiocare;
	private Check controlligenerici;
	private Continent continenti;
	private Attack attacco;
	private CheckTargets controlloobbiettivi;
	private CounterStartTurn conteggioarmateinizioturno;
	private static List<Land> listaterritori;
	private static LandsListCreator creatorelista;

	/**
	 * 
	 * @param terr
	 *            is the list of country.
	 */
	public ActionsImpl() {
		creatorelista = new LandsListCreator();
		listaterritori = creatorelista.getListaTerritori();
		continenti = Continent.getContinent(listaterritori);
		controlligenerici = Check.getCheck(listaterritori);
		obbiettivi = TargetsArrayCreator.getTargetsImpl(Controller.getPlayers().size(), continenti);
		attacco = Attack.getAttack();
		controlloobbiettivi = CheckTargets.getCheckTargets(obbiettivi);
		conteggioarmateinizioturno = CounterStartTurn.getCST(continenti);
		fasiprimadigiocare = Controller.getPlayers().size();
		assegnatoreRandomTerritoriInizioPartita(fasiprimadigiocare);
		aggiornamentoMappa();
		nuovoTurno(0);
	}

	/**
	 * 
	 * @param terr
	 *            is the list of country.
	 * @return if actions has already been created.
	 */
	public static Actions getActions() {
		if (singleton == null) {
			singleton = new ActionsImpl();
		}
		return singleton;
	}

	@Override
	public void nuovoTurno(final int combo) {

		Controller.activateButtonsPlayerInTurn();
		Controller.deactivateSpostamento();
		Controller.deactivateFineTurno();
		Controller.updateObbiettivi(controlloobbiettivi.getString(Controller.getActualPlayer().getColore()));
		if (intentodelplayer == PRIMOTURNO) {
			Controller.updateComandi(MESSAGGIODIPOSIZIONAMENTO);
			setArmateInizioTurno(Controller.getActualPlayer());
		} else if (fasiprimadigiocare > 0) {
			fasiprimadigiocare--;
			intentodelplayer = primasceltaattacco;
			Controller.activateSpostamento();
			Controller.activateFineTurno();
			controlligenerici.intent3(controlligenerici);
		} else if (fasiprimadigiocare == 0) {
			Controller.updateComandi(MESSAGGIODIPOSIZIONAMENTO);
			intentodelplayer = POSIZIONAMENTOINIZIOTURNO;
			setArmateDaAggiungere(combo);
		}
	}

	/**
	 * 
	 * @param p
	 *            is the actual player in turn.
	 */
	public void setArmateInizioTurno(final Player p) {
		armatemancanti = conteggioArmateInizioGioco(getTerritori(), Controller.getPlayers().size(), p);
		controller.Controller.updateLeftarmies("" + armatemancanti);
	}

	/**
	 * 
	 * @return the number of armies left to add.
	 */
	public boolean posizionamentoInizioTurnoArmate() {
		armatemancanti--;
		Controller.updateLeftarmies("" + armatemancanti);
		aggiornamentoMappa();
		return armatemancanti == 0;
	}

	/**
	 * 
	 * @param combo
	 *            the number of armies to add by cards .
	 */
	public void setArmateDaAggiungere(final int combo) {
		armatedaaggiungere = (conteggioarmateinizioturno.CST(listaterritori, Controller.getActualPlayer()));

		if (combo > 0) {
			Controller.updateLeftarmies(armatedaaggiungere + " + " + combo);
			armatedaaggiungere += combo;
		} else {
			Controller.updateLeftarmies("" + armatedaaggiungere);
		}
	}

	/**
	 * 
	 * @return the number of armies left to add.
	 */
	public boolean controlloArmateDaAggiungere() {
		armatedaaggiungere--;
		controller.Controller.updateLeftarmies("" + armatedaaggiungere);
		return armatedaaggiungere == 0;
	}

	@Override
	public void setIntentoPlayerSpostamento() {
		intentodelplayer = primasceltaspostamento;
		controlligenerici.intent5();
		Controller.deactivateSpostamento();
	}

	@Override
	public void dirottatoreSensoPulsantePremuto(final int code) {

		switch (intentodelplayer) {

		case PRIMOTURNO:

			controlligenerici.place(listaterritori.get(code - 1), 1);
			if (posizionamentoInizioTurnoArmate()) {
				if (Controller.getActualPlayer() == Controller.getPlayers().getLast()) {
					intentodelplayer = primasceltaattacco;
					aggiornamentoMappa();
					Controller.nextPlayer();
				} else {
					aggiornamentoMappa();
					Controller.nextPlayer();
				}
			}
			break;

		case POSIZIONAMENTOINIZIOTURNO:

			controlligenerici.place(listaterritori.get(code - 1), 1);
			aggiornamentoMappa();

			if (controlloArmateDaAggiungere()) {
				Controller.activateSpostamento();
				Controller.activateFineTurno();
				aggiornamentoMappa();
				if (controlligenerici.hasOneMinCountryCanAttak()) {
					intentodelplayer = primasceltaattacco;
					controlligenerici.intent3(controlligenerici);
					aggiornamentoMappa();
				} else if (controlligenerici.hasOneMinCountryCanMove()) {
					intentodelplayer = primasceltaspostamento;
					controlligenerici.intent5();
					aggiornamentoMappa();
				} else {
					Controller.deactivateAllButtons();
					Controller.deactivateSpostamento();
					aggiornamentoMappa();
				}
			}

			break;

		case primasceltaattacco:

			numerostatiprimaattacco = (int) getTerritori().stream()
					.filter(a -> a.getOwner() == Controller.getActualPlayer().getColore()).count();
			intentodelplayer = secondasceltaattacco;
			controlligenerici.intent4(listaterritori.get(code - 1));
			codicestatoattaccante = code;
			aggiornamentoMappa();
			break;

		case secondasceltaattacco:

			attacco.atk(listaterritori, codicestatoattaccante, code, Controller.getPlayers(), controlligenerici);
			numerostatidopoattacco = (int) listaterritori.stream()
					.filter(a -> a.getOwner() == Controller.getActualPlayer().getColore()).count();

			aggiornamentoMappa();
			if (numerostatidopoattacco > numerostatiprimaattacco) {
				Controller.draw();
				controlloobbiettivi.win(listaterritori, Controller.getActualPlayer(), controlligenerici);
				if (listaterritori.get(codicestatoattaccante - 1).getNumArmies() > 1) {
					intentodelplayer = posizionamentodopoattacco;
					controlligenerici.intent7(codicestatoattaccante, code);
					codicestatospostante = code;
				} else {

					if (controlligenerici.hasOneMinCountryCanAttak()) {
						intentodelplayer = primasceltaattacco;
						controlligenerici.intent3(controlligenerici);
					} else if (controlligenerici.hasOneMinCountryCanMove()) {
						intentodelplayer = primasceltaspostamento;
						controlligenerici.intent5();
					} else {
						Controller.deactivateAllButtons();
						Controller.deactivateSpostamento();
					}
				}
			} else {
				if (controlligenerici.hasOneMinCountryCanAttak()) {
					intentodelplayer = primasceltaattacco;
					controlligenerici.intent3(controlligenerici);
				} else if (controlligenerici.hasOneMinCountryCanMove()) {
					intentodelplayer = primasceltaspostamento;
					controlligenerici.intent5();
				} else {
					Controller.deactivateAllButtons();
					Controller.deactivateSpostamento();
				}
			}

			break;

		case primasceltaspostamento:
			codicestatospostante = code;
			intentodelplayer = secondasceltaspostamento;
			Controller.deactivateSpostamento();
			controlligenerici.intent6(listaterritori.get(code - 1));
			aggiornamentoMappa();
			break;

		case secondasceltaspostamento:
			controlligenerici.move(codicestatospostante, code, 1);
			aggiornamentoMappa();
			Controller.deactivateAllButtons();
			Controller.activateButton(code);

			if (listaterritori.get(codicestatospostante - 1).getNumArmies() < 2) {
				intentodelplayer = POSIZIONAMENTOINIZIOTURNO;
				Controller.deactivateAllButtons();
			}

			break;

		case posizionamentodopoattacco:

			aggiornamentoMappa();
			if (code != codicestatospostante) {
				if (controlligenerici.hasOneMinCountryCanAttak()) {
					intentodelplayer = primasceltaattacco;
					controlligenerici.intent3(controlligenerici);
				} else if (controlligenerici.hasOneMinCountryCanMove()) {
					intentodelplayer = primasceltaspostamento;
					controlligenerici.intent5();
				} else {
					Controller.deactivateAllButtons();
					Controller.deactivateSpostamento();
				}

			} else {
				controlligenerici.move(codicestatoattaccante, code, 1);
				aggiornamentoMappa();
				if (listaterritori.get(codicestatoattaccante - 1).getNumArmies() < 2) {
					if (controlligenerici.hasOneMinCountryCanAttak()) {
						intentodelplayer = primasceltaattacco;
						controlligenerici.intent3(controlligenerici);
					} else if (controlligenerici.hasOneMinCountryCanMove()) {
						intentodelplayer = primasceltaspostamento;
						controlligenerici.intent5();
					} else {
						Controller.deactivateAllButtons();
						Controller.deactivateSpostamento();
					}
				}
			}

			break;

		default:
			// c'è qualcosa che non va
			break;
		}
	}

	/**
	 * 
	 * @return the list of country.
	 */
	public List<Land> getTerritori() {
		return listaterritori;
	}

	/**
	 * 
	 * @return the array of targets.
	 */
	public TargetsArrayCreator getObbiettivi() {
		return obbiettivi;
	}

	/**
	 * 
	 * @param totplayers.
	 */
	public void assegnatoreRandomTerritoriInizioPartita(final int totplayers) {

		int each = totaleterritori / totplayers;
		int rest = totaleterritori % totplayers;
		int check = 0;

		Random rand = new Random();
		for (int i = 1; i <= totplayers; i++) {
			for (int j = 0; j < each; j++) {
				check = rand.nextInt(totaleterritori) + 0;
				if (listaterritori.get(check).getOwner() != 0) {
					j--;
				} else {
					listaterritori.get(check).setOwner(i);
				}
			}
		}
		each = 0;
		for (Land a : listaterritori) {
			if (a.getOwner() == 0) {
				break;
			}
			each++;
		}
		if (each != totaleterritori) {

			check = rand.nextInt(totplayers) + 1;
			listaterritori.get(each).setOwner(check);
			for (each += 1; listaterritori.get(each).getOwner() != 0; each++) {
			}
			do {
				rest = rand.nextInt(totplayers) + 1;
			} while (rest == check);
			listaterritori.get(each).setOwner(rest);
		}

		intentodelplayer = PRIMOTURNO;
	}

	/**
	 * 
	 */
	public void aggiornamentoMappa() {

		for (int i = 0; i < totaleterritori; i++) {
			Controller.updateArmiesButton(i, listaterritori.get(i).getNumArmies());
			Controller.updateColorButton(i, listaterritori.get(i).getOwner());
		}
	}

	/**
	 * 
	 * @param terr
	 *            is the list of bottons country .
	 * @param totplayers
	 *            is the size of the players list .
	 * @param p
	 *            is the actual player in turn .
	 * @return the number of armies to add.
	 */
	public int conteggioArmateInizioGioco(final List<Land> terr, final int totplayers, final Player p) {

		int leftArmies = 0;
		int usedArmies = terr.stream().filter(a -> a.getOwner() == p.getColore()).toArray().length;

		switch (totplayers) {

		case treplayers:
			leftArmies = (armatecadaunotregiocatori - usedArmies);
			break;

		case quattroplayers:
			leftArmies = (armatecadaunoquattrogiocatori - usedArmies);
			break;

		case cinqueplayers:
			leftArmies = (armatecadaunocinquegiocatori - usedArmies);
			break;

		case seiplayers:
			leftArmies = (armatecadaunoseigiocatori - usedArmies);
			break;

		default:
			break;
		}

		return leftArmies;

	}
}
