package model;

import java.util.LinkedList;
import java.util.List;

public class LandsListCreator {
	
	private List<Land> listaterritori = new LinkedList<Land>();

	public LandsListCreator() {

		for (int i = 0; i < 42; i++) {
            listaterritori.add(new LandImpl(LandsData.values()[i].getNome(),
                    LandsData.values()[i].getCode(),LandsData.values()[i].getConfini()));
    	}
	}
	
	public List<Land> getListaTerritori() {
		return listaterritori;
	}
}
