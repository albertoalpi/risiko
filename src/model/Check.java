package model;

import java.util.LinkedList;
import java.util.List;

import controller.Controller;

/**
 * 
 * this class contains all the checks that the model uses in all his class .
 *
 */
public final class Check {

	private final String FATK = "Premi lo stato con cui vuoi attaccare";
	private final String SATK = "Premi lo stato che vuoi attaccare";
	private final String FMOV = "Premi stato da cui vuoi spostare le armate";
	private final String SMOV = "Premi lo stato dove vuoi spostare le armate";
	private final String MOVSTOP = "Premi: stato conquistato per spostare un'armata / stato attaccante per annullare lo spostamento";
	private static Check singleton;	
	private List<Land> terr;
	
	/**
	 * 
	 * @param territori
	 */
	public Check(final List<Land> territori) {
		terr = territori;
	}
	
	
	/**
	 * 
	 * @return if Check has already been created.
	 */
    public static Check getCheck(final List<Land> territori) {
		if (singleton == null) {
			singleton = new Check(territori);
		}
		return singleton;
	}

	/**
	 * 
	 * @param a is the botton where the player is placing the armies .
	 * @param tot is how many armies the player is placing .
	 */
	public void place(final Land a, final int tot) {
		a.setNumArmies(a.getNumArmies() + tot);
	}

	/**
	 * @param a is a country .
	 * @return true if a has two or more armies .
	 */
	public boolean minNumArmiesToAtk(final Land a) {
		return a.getNumArmies() > 1;
	}

	/**
	 * @param a is the out_move country .
	 * @param b is the in_move country.
	 * @param tot is the number of armies moved.
	 */
	public void move(final int a, final int b, final int tot) {
		terr.get(b - 1).setNumArmies((terr.get(b - 1).getNumArmies() + tot));
		terr.get(a - 1).setNumArmies((terr.get(a - 1).getNumArmies() - tot));
	}

	/**
	 * 
	 * @param b .
	 * @return .
	 */
	public boolean isDead(final int b) {
		return terr.get(b - 1).getNumArmies() == 0;
	}

	/**
	 * @param a is a country .
	 * @param b is a country .
	 * @return true if has different owner .
	 */
	public boolean differentOwner(final Land a, final Land b) {
		return a.getOwner() != b.getOwner();
	}
	
	/**
	 * 
	 * @return true if the actual player in turn has min one country can attack else false.
	 */
	public boolean hasOneMinCountryCanAttak() {
		for (Land a : terr) {
			if (a.getOwner() == Controller.getActualPlayer().getColore() && hasOneMinDifferentOwnerBorder(a) && minNumArmiesToAtk(a)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 
	 * @return true if the actual player in turn has min one country can move else false.
	 */
	public boolean hasOneMinCountryCanMove() {
		for (Land a : terr) {
			if (a.getOwner() == Controller.getActualPlayer().getColore() && hasOneMinEqualOwnerBorder(a) && a.getNumArmies() > 1) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 
	 * @param a is a country.
	 * @return true if a has min one enemy border else false.
	 */
	public boolean hasOneMinDifferentOwnerBorder(final Land a) {
		for (int i = 0; i < a.getConfini().length; i++) {
			if (Controller.getActualPlayer().getColore() != terr.get(a.getConfini()[i] - 1).getOwner()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @param a is a country.
	 * @return true if a has min one friend border else false.
	 */
	public boolean hasOneMinEqualOwnerBorder(final Land a) {
		for (int i = 0; i < a.getConfini().length; i++) {
			if (Controller.getActualPlayer().getColore() == terr.get(a.getConfini()[i] - 1).getOwner()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @controlli. ciao.
	 * this method activate only the country that can can attack .
	 */
	public void intent3(final Check controlli) {

		for (Land a : terr) {
			if (Controller.getActualPlayer().getColore() == a.getOwner() && a.getNumArmies() > 1
					&& controlli.hasOneMinDifferentOwnerBorder(a)) {
				Controller.activateButton(a.getCode());
			} else {
				Controller.deactivateButton(a.getCode());
			}
		}
		controller.Controller.updateComandi(FATK);
	}

	/**
	 * 
	 * @param a is a country.
	 * this method activate only country can attacked by a .
	 */
	public void intent4(final Land a) {
		Controller.deactivateAllButtons();
		for (int i = 0; i < a.getConfini().length; i++) {
			if (a.getOwner() != terr.get(a.getConfini()[i] - 1).getOwner()) {
				Controller.activateButton(terr.get(a.getConfini()[i] - 1).getCode());
			}
		}
		Controller.updateComandi(SATK);
	}

	/**
	 * this method activate only country can move .
	 */
	public void intent5() {
		for (Land a : terr) {
			if ((a.getOwner() == Controller.getActualPlayer().getColore()) && (hasOneMinEqualOwnerBorder(a)) && (a.getNumArmies() > 1)) {
				Controller.activateButton(a.getCode());
			} else {
				Controller.deactivateButton(a.getCode());
			}
		}
		Controller.updateComandi(FMOV);
	}

	/**
	 * 
	 * @param a is a country.
	 * this method activate only country can in_move by a .
	 */
	public void intent6(final Land a) {
		Controller.deactivateAllButtons();
		for (int i = 0; i < a.getConfini().length; i++) {
			if (a.getOwner() == terr.get(a.getConfini()[i] - 1).getOwner()) {
				Controller.activateButton(terr.get(a.getConfini()[i] - 1).getCode());
			}
		}
		Controller.updateComandi(SMOV);
	}

	/**
	 * 
	 * @param a is a country.
	 * @param b is a country.
	 * this method activate only two country where armies is moving .
	 */
	public void intent7(final int a, final int b) {
		Controller.deactivateAllButtons();
		Controller.activateButton(terr.get(a - 1).getCode());
		Controller.activateButton(terr.get(b - 1).getCode());
		Controller.updateComandi(MOVSTOP);
	}
}
