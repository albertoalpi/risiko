package model;

import java.util.LinkedList;
import java.util.List;

import players.Player;

/**
 * this class describe the six method with the specific checks 
 * to define if a continent is totally possessed by one player . 
 *
 */
public class Continent {

	private final int NUMSTATSAMERICAN = 9;
	private final int NUMSTATSAMERICAS = 4;
	private final int NUMSTATSAFRICA = 6;
	private final int NUMSTATSOCEANIA = 4;
	private final int NUMSTATSASIA = 12;
	private final int NUMSTATSEUROPA = 7;
	private int check;
	private List<Land> terr;
	private static Continent singleton;
	
	
	/**
	 * 
	 */
    public Continent(final List<Land> territori) {
		terr = territori;
	}
    
	/**
	 * 
	 */
    public static Continent getContinent(final List<Land> territori) {
		if (singleton == null) {
			singleton = new Continent(territori);
		}
		return singleton;
	}

	/**
	 * 
	 * @param terr is the list of bottons country .
	 * @param p is the actual player in turn .
	 * @return true if NordAmerica is totally owned by p else false .
	 */
	public boolean totalAmericaN(final List<Land> terr, final Player p) {
		check = 0;
		for (int i = 0; i < NUMSTATSAMERICAN; i++) {
			if (p.getColore() == terr.get(i).getOwner()) {
				check++;
			}
		}
		return check == NUMSTATSAMERICAN;
	}
	
	/**
	 * 
	 * @param terr is the list of bottons country .
	 * @param p is the actual player in turn .
	 * @return true if SudAmerica is totally owned by p else false .
	 */
	public boolean totalAmericaS(final List<Land> terr, final Player p) {
		check = 0;
		for (int i = NUMSTATSAMERICAN; i < (NUMSTATSAMERICAN + NUMSTATSAMERICAS); i++) {
			if (p.getColore() == terr.get(i).getOwner()) {
				check++;
			}
		}
		return check == NUMSTATSAMERICAS;
	}

	/**
	 * 
	 * @param terr is the list of bottons country .
	 * @param p is the actual player in turn .
	 * @return true if Europa is totally owned by p else false .
	 */
	public boolean totalEuropa(final List<Land> terr, final Player p) {
		check = 0;
		for (int i = (NUMSTATSAMERICAN + NUMSTATSAMERICAS); i < (NUMSTATSAMERICAN + NUMSTATSAMERICAS + NUMSTATSEUROPA); i++) {
			if (p.getColore() == terr.get(i).getOwner()) {
				check++;
			}
		}
		return check == NUMSTATSEUROPA;
	}

	/**
	 * 
	 * @param terr is the list of bottons country .
	 * @param p is the actual player in turn .
	 * @return true if Africa is totally owned by p else false .
	 */
	public boolean totalAfrica(final List<Land> terr, final Player p) {
		check = 0;
		for (int i = (NUMSTATSAMERICAN + NUMSTATSAMERICAS + NUMSTATSEUROPA); i < (NUMSTATSAMERICAN + NUMSTATSAMERICAS + NUMSTATSEUROPA + NUMSTATSAFRICA); i++) {
			if (p.getColore() == terr.get(i).getOwner()) {
				check++;
			}
		}
		return check == NUMSTATSAFRICA;
	}

	/**
	 * 
	 * @param terr is the list of bottons country .
	 * @param p is the actual player in turn .
	 * @return true if Asia is totally owned by p else false .
	 */
	public boolean totalAsia(final List<Land> terr, final Player p) {
		check = 0;
		for (int i = (NUMSTATSAMERICAN + NUMSTATSAMERICAS + NUMSTATSEUROPA 
				+ NUMSTATSAFRICA); i < (NUMSTATSAMERICAN + NUMSTATSAMERICAS 
						+ NUMSTATSEUROPA + NUMSTATSAFRICA + NUMSTATSASIA); i++) {
			if (p.getColore() == terr.get(i).getOwner()) {
				check++;
			}
		}
		return check == NUMSTATSASIA;
	}

	/**
	 * 
	 * @param terr is the list of bottons country .
	 * @param p is the actual player in turn .
	 * @return true if Oceania is totally owned by p else false .
	 */
	public boolean totalOceania(final List<Land> terr, final Player p) {
		check = 0;
		for (int i = (NUMSTATSAMERICAN + NUMSTATSAMERICAS + NUMSTATSEUROPA 
				+ NUMSTATSAFRICA + NUMSTATSASIA); i < (NUMSTATSAMERICAN 
						+ NUMSTATSAMERICAS + NUMSTATSEUROPA + NUMSTATSAFRICA 
						+ NUMSTATSASIA + NUMSTATSOCEANIA); i++) {
			if (p.getColore() == terr.get(i).getOwner()) {
				check++;
			}
		}
		return check == NUMSTATSOCEANIA;
	}

	
}
