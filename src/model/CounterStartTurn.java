package model;

import java.util.LinkedList;
import java.util.List;

import model.Continent;
import players.Player;

/**
 * 
 * this class count the number of armies to add in a normal turn 
 * checking the continent and the numbers of country .
 */
public class CounterStartTurn {

	private static int armiesToAdd = 0;
	private final int AmericaN = 5;
	private final int AmericaS = 2;
	private final int Europa = 5;
	private final int Africa = 3;
	private final int Asia = 7;
	private final int Oceania = 2;
	private Continent cont;
	private static CounterStartTurn singleton;

	public CounterStartTurn(final Continent continenti) {
		cont = continenti;
	}
	
	public static CounterStartTurn getCST(final Continent continenti) {
		if (singleton == null) {
			singleton = new CounterStartTurn(continenti);
		}
		return singleton;
	}
	/**
	 * 
	 * @param listaterritori is the list of bottons country .
	 * @param p is the actual player in turn .
	 * @return the number of armies to add .
	 */
	public int CST(final List<Land> listaterritori, final Player p) {

		armiesToAdd = 0;

		int numtot = 0;

		for (Land a: listaterritori) {

			if (a.getOwner() == p.getColore()) {
				numtot++;
			}
		}

		armiesToAdd = numtot / 3;

		if (cont.totalAmericaN(listaterritori, p)) {
			armiesToAdd += AmericaN;
		}

		if (cont.totalAmericaS(listaterritori, p)) {
			armiesToAdd += AmericaS;
		}

		if (cont.totalEuropa(listaterritori, p)) {
			armiesToAdd += Europa;
		}

		if (cont.totalAfrica(listaterritori, p)) {
			armiesToAdd += Africa;
		}

		if (cont.totalAsia(listaterritori, p)) {
			armiesToAdd += Asia;
		}

		if (cont.totalOceania(listaterritori, p)) {
			armiesToAdd += Oceania;
		}

		return armiesToAdd;

	}

}
